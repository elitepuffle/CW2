#ifndef STRING_H
#define STRING_H

typedef char vstring[100][256];

void upperCase(char[] string);

bool isPunc(char c);
void cleanString(char[] string);
void copy(char[] string, vstring &v, size_t array_size);
void upperCase(char[] string);
void space(char[] string);
void trimL(char[] string, char[] delim);
void trimR(char[] string, char[] delim);
size_t replace(char[] string, char[] substr1, char[] substr2);





#endif